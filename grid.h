/*******************************************************************
  File name:  grid.h
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/

#ifndef __GRID_H__
#define __GRID_H__

typedef struct
{
	int** index; //2D array of indexes in one chunk
}oneChunk;

typedef struct
{
	int xLen; //number of chunks in x range of display
	int yLen; //number of chunks in y range of display
	int chWidth; //8 width of one chunk
	int chHeight; //8 //height of one chunk
	oneChunk* chunks;
}lineChunk;

/************************************************************
Function used to init struct able to hold
indexes of current "grid" line

@width   = proportions of one chunk in x range
@height  = proportions of one chunk in y range
************************************************************/
lineChunk* gridInit(int width, int height);

/************************************************************
Function used to fill "map" with display indexes in current "line"

@line = row of grid (display cut to chunks)
@map  = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
************************************************************/
void gridIndex(lineChunk* map, int line);

/************************************************************
Function used to free all dynamically allocated memmory
associated with "map"

@map  = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
************************************************************/
void gridFree(lineChunk* map);

#endif
