/*******************************************************************
  File name:  ledLine.h
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/
#ifndef __LEDLINE_H__
#define __LEDLINE_H__

#include <stdint.h>

/************************************************************
Function used to initialize local struct values and load led 
line bar at program start

@mem_base  = mapped registers, allowing access to RGB and ledLine
************************************************************/
void initLed(unsigned char *mem_base);

/************************************************************
Function used to turn off led line from program
************************************************************/
void turnOfLed(void);

/************************************************************
Function used to visualize progress bar of point collection
Calculates percentage of points needed to light up one led
and percentage given by one point collection

@maxPts  = maximum points in game, selected by player, default 25
************************************************************/
void visualizePts(int maxPts);

/************************************************************
Function used to turn off RGBs from program
************************************************************/
void turnOfRGB(void);

/************************************************************
Function used as thread maintaining RGB signalization
Ends when global flag for game end is set
Thread detaches itself before end

@arg  = NULL
************************************************************/
void* rgbThread(void* arg);

/************************************************************
Function used for telling rgb thread to start working
************************************************************/
void setRGB(int opt);

/************************************************************
Function used for telling rgb thread to stop working
************************************************************/
void setNotRGB(void);

#endif

//------------END_LEDLINE_H------------