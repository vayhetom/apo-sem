/*******************************************************************
  File name:  ledLine.c
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/
 
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "maps.h"
#include "ledLine.h"
#include "gameAi.h"
#include "utils.h"

int isRGB(void);
void dangerRGB(int opt);
void pointRGB(void);
void gameStartRGB(void);

static struct {
    unsigned char *memBase;
    uint32_t valLine;
    int rgb;
    pthread_mutex_t mtx;
} led = {.memBase = NULL, .valLine = 0, .rgb = 0};

/************************************************************
Function used to initialize local struct values and load led 
line bar at program start

@mem_base  = mapped registers, allowing access to RGB and ledLine
************************************************************/

void initLed(unsigned char *mem_base)
{
    pthread_mutex_init(&(led.mtx), NULL);
    led.memBase = mem_base;
    led.valLine = 0;
    struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 20 * 1000 * 1000};
    for (int i = 1; i <= 30; i++) {
        led.valLine += 1 << i;
 		*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_LINE_o) = led.valLine;
        clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
    }
}
/************************************************************
Function used to turn off led line from program
************************************************************/
void turnOfLed(void)
{
    led.valLine = 0;
    *(volatile uint32_t *)(led.memBase + SPILED_REG_LED_LINE_o) = led.valLine;
}

/************************************************************
Function used to visualize progress bar of point collection
Calculates percentage of points needed to light up one led
and percentage given by one point collection

@maxPts  = maximum points in game, selected by player, default 25
************************************************************/
void visualizePts(int maxPts)
{
    int leftPts = sendPoints();
    int currentPts = maxPts - leftPts;
    float oneLed = 3.33; // one led = 3.33 percent of maxPts
    float incrProg = 100 / maxPts;  //point progress per collected point
    int range = (currentPts * incrProg) / oneLed;
    range = 30 - range;
    led.valLine = 0;
    for (int i = 30; i > range; --i) {
        led.valLine += 1 << i;
        *(volatile uint32_t *)(led.memBase + SPILED_REG_LED_LINE_o) = led.valLine;
    }
}

/************************************************************
Function used as thread maintaining RGB signalization
Ends when global flag for game end is set
Thread detaches itself before end

@arg  = NULL
************************************************************/
void* rgbThread(void* arg)
{
	bool quit = isQuit();
	setNotRGB();
	gameStartRGB();
	while(!quit)
	{
		while(!isRGB())
		{
			if(isQuit())
			{
				break;
			}
			usleep(100 * 1000); //10ms
		}
		int rgb = isRGB();
		if(rgb == 1)
		{	
			pointRGB();
			setNotRGB();
		}
		if(rgb >1 && rgb < 5)
		{
			dangerRGB(rgb);
		}
		quit = isQuit();
	}
	pthread_detach(pthread_self());
	turnOfRGB();
	info("Exit RGB Thread");
	return NULL;
}

/************************************************************
Function used to visualize game start with 5 blue flashes
************************************************************/
void gameStartRGB(void)
{
	for(int i = 0; i < 5; ++i)
	{
		*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_RGB1_o) = 0xff; //B
		*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_RGB2_o) = 0xff;
		usleep(250 * 1000); //100 ms
		*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_RGB1_o) = 0; //Black
		*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_RGB2_o) = 0;
		usleep(250 * 1000);
	}
}

/************************************************************
Function used to visualize point collection with 2 yellow flashes
************************************************************/
void pointRGB(void)
{
	for(int i = 0; i < 2; ++i)
	{
		*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_RGB1_o) = 0xffff00; //Yellow
		*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_RGB2_o) = 0xffff00;
		usleep(100 * 1000); //100 ms
		*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_RGB1_o) = 0; //Black
		*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_RGB2_o) = 0;
		usleep(100 * 1000);
	}
}

/************************************************************
Function used to visualize danger from AI in proximity of 2 
turns with red flashes and frequency depending on distance
************************************************************/
void dangerRGB(int opt)
{
	for(int i = 0; i < 2; ++i)
	{
		*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_RGB1_o) = 0xff0000; //Yellow
		*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_RGB2_o) = 0;
		usleep(75 * opt * 1000); //100 ms
		*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_RGB1_o) = 0; //Black
		*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_RGB2_o) = 0xff0000;
		usleep(75 * opt  * 1000);
		*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_RGB2_o) = 0;
	}
}

/************************************************************
Function used to turn off RGBs from program
************************************************************/
void turnOfRGB(void)
{
	*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_RGB1_o) = 0;
	*(volatile uint32_t*)(led.memBase + SPILED_REG_LED_RGB2_o) = 0;
}


/************************************************************
 -----------------SYNCHRONIZATION_BLOCK----------------------
 ************************************************************/
void setNotRGB(void)
{
	pthread_mutex_lock(&(led.mtx));
	led.rgb = 0;
	pthread_mutex_unlock(&(led.mtx));
}

void setRGB(int opt)
{
	pthread_mutex_lock(&(led.mtx));
	led.rgb = opt;
	pthread_mutex_unlock(&(led.mtx));
}

int isRGB(void)
{
	pthread_mutex_lock(&(led.mtx));
	int ret = led.rgb;
	pthread_mutex_unlock(&(led.mtx));	
	return ret;
}
/************************************************************
 ----------------END_SYNCHRONIZATION_BLOCK-------------------
 ************************************************************/

//------------END_LEDLINE_C------------
