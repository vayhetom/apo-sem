/*******************************************************************
  File name:  gameAi.h
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/
#ifndef __GAMEAI_H__
#define __GAMEAI_H__

#include <stdbool.h>
#include "grid.h"

/************************************************************
Function used as a thread for "aiFirst" 
movement and synchronization

@arg  = array of RGB values of display

Thread is detached before return
************************************************************/
void* logicAi(void* arg);

/************************************************************
Function used as a thread for "aiSecond" 
movement and synchronization

@arg  = array of RGB values of display

Thread is detached before return
************************************************************/
void* logicSecondAi(void* arg);

/************************************************************
Function used to init mutexes and local "aiFirst" representation 
to starting position and send signal to draw AI "player"

@fb   = array of RGB values of display
@map  = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
************************************************************/
void initAi(unsigned short *fb, lineChunk* map);

/************************************************************
Function used to local "aiSecond" representation to starting
position and send signal to draw AI "player"

@fb   = array of RGB values of display
@map  = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
************************************************************/
void initSecond(unsigned short *fb, lineChunk* map);

/************************************************************
@pacMan = struct holding information abou player
@aiRow  = Y position of AI on the board
@aiCol  = X position of AI on the board
@ret    = returns squared vector distance between player and AI
************************************************************/
int calcDist(locPlayer* pacMan, int aiRow, int aiCol);

/************************************************************
 -----------------SYNCHRONIZATION_BLOCK----------------------
 ************************************************************/
void setNotMove(void); 

void setMove(void);

bool isMove(void);

void setNotQuit(void);

bool isMove2(void);

void setMedAi(void);

void setNotMedAi(void);

void setQuit(void);

bool isQuit(void);

locPlayer* sendPlayer(void);

locPlayer* sendPlayer2(void);
/************************************************************
 ----------------END_SYNCHRONIZATION_BLOCK-------------------
 ************************************************************/

#endif
//------------END_GAME_AI_H------------