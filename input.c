/*******************************************************************
  File name:  input.c
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdbool.h>
#include "input.h"
#include "utils.h"
#include "game.h"
#include "menuFunc.h"
#include "gameAi.h"
#include "ledLine.h"

/**********************************************************
	Start of program logic - Menu 
	@base = parlcd_mem_base adresses for display
	@fb = pixel values for display
	
	base and fb are only forwarded towards other functions
***********************************************************/

static settings set;
    
void inputThr(unsigned char* base, unsigned short* fb)
{
	call_termios(0);
	int c;
	settingsInit(&set);	
	mapInit1();
	mapInit2();
	display("main menu", base, fb);	
	printMan();
	fprintf(stderr, "------------------MAIN-MENU--------------------\n");
	info("Main Menu: Waiting for input.");
	pthread_t aiThr;
	pthread_t aiSecThr;
	pthread_t rgbThr;
	while((c = getchar()) != 'q')
	{
		if(c == 'Q')
		{
			break; //process to program exit
		}
		switch(c)
		{
			case 'N':
			case 'n':
			{
			    selection("main menu", 0, base, fb);
				pthread_create(&aiThr, NULL, logicAi, fb);
				pthread_create(&rgbThr, NULL, rgbThread, NULL);
				if(set.ai == AI_ADVANCED || set.ai == AI_HARD)
				{
					pthread_create(&aiSecThr, NULL, logicSecondAi, fb);
				}
				//New game
				newGame(&set, base, fb);
				info("AI Thread was joined");
				if (getOutcome() != 2)
				{
				 finalScreen(base, fb);
				}
				display("main menu", base, fb);	
				printMan();
				setNotQuit();
				break;
			}
			case 'M': //map selection
			case 'm':
			{
			    selection("main menu", 1, base, fb);
			    printMaps(1, fb, base);
				mapSelect(&set, base, fb);
				break;
			}
			case 'A': //ai selection
			case 'a':
			{
			    selection("main menu", 2, base, fb);
			    display("set ai", base, fb);
				aiSelect(&set, base, fb);
				break;
			}
			case 'P': //point selection
			case 'p':
			{
			    selection("main menu", 3, base, fb);
			    display("set points", base, fb);
				pointSelect(&set, base, fb);
				break;
			}
			case 'H': //manual print
			case 'h':
			{
				printMan();
				break;
			}
			default:
			{
			    info("Wrong input!");
			    break;
			}
		}
		info("Main Menu: Waiting for input.");
	}
	selection("main menu", 4, base, fb);
	mapFree1();
	mapFree2();
	call_termios(1);
}

/************************************************************
Utility method for game settings initialization
*************************************************************/ 
void settingsInit(settings* set)
{
	set->map = MAP_FIRST;
	set->points = 25; 
	set->ai = AI_EASY;
}

/************************************************************
Utility method to send amount of points to other files
Retruns number of points
*************************************************************/ 
int getPts(void){
    return set.points;
}

/************************************************************
This method prints out game outcome
*************************************************************/ 
void finalScreen(unsigned char* base, unsigned short* fb){
    if (getOutcome() == 1){
        display("you won", base, fb);
    } else {
        display("game over", base, fb);
    }
    
    char c;
    bool exit = false;
    while (!exit){
        if ((c = getchar()) == 'b' || (c = getchar()) == 'B'){
            exit = true;
        }
    }
}

/************************************************************
This method opens up a point selection menu for the user
@set - settings struct
*************************************************************/ 
void pointSelect(settings* set, unsigned char* base, unsigned short* fb)
{
	fprintf(stderr, "----------------POINT-SELECTION-----------------\n");
	info("Select amount of points on board min 10, max 50");
	info("Press '+' for +5 points");
	info("Press '-' for -5 points");
	info("Press 'B' to get back to Main Menu");
	char in;
	bool exit = false;

	do
	{	
		in = getchar();
		switch(in)
		{
			case '+': //increment points
			{
				if(set->points < 50)
				{
					set->points += 5;
					display("points change", base, fb);
				}
				fprintf(stderr, "Points: %d\n", set->points);
				break;
			}
			case '-': //decrement points
			{
				if(set->points > 10)
				{
					set->points -= 5;
					display("points change", base, fb);
				}
				fprintf(stderr, "Points: %d\n", set->points);
				break;
			}
			case 'B': //go back to main menu
			case 'b':
			{
				selection("set points", 4, base, fb);
				exit = true;
				break;
			}
			default:
			{
				info("WARN: Wrong key pressed!");
		    	break;
			}
		}
		
	}while(!exit);
	display("main menu", base, fb);
	fprintf(stderr, "------------------MAIN-MENU--------------------\n");
}

/************************************************************
This method opens up a map selection menu for the user
@set - settings struct
*************************************************************/ 
void mapSelect(settings* set, unsigned char* base, unsigned short* fb)
{
    fprintf(stderr, "-----------------MAP-SELECTION----------------\n");
	info("Browse maps with keys 'A' and 'D'");
	info("Press 'S' to select map");
	info("Press 'B' to get back to Main Menu");
	int map;
	int mapType = 1;
	bool exit = false;
	
	do
	{
	    map = getchar();
	    if (map == 'B')
	    {
	        break;
	    }
	    switch(map)
	    {
	        case 'b': //back to main menu
	        case 'B':
	            selection("set map", 2, base, fb);
    			display("main menu", base, fb);
    			exit = true;
	            break;
	        case 's': //select the chosen map
	        case 'S':
	        {
	            if (mapType == 1)
	            {
    	            set->map = MAP_FIRST;
	            } 
	            else if (mapType == 2)
	            {
	                set->map = MAP_SECOND;
	            } 
	            else if (mapType == 3)
	            {
	                set->map = MAP_THIRD;
	            }
    			fprintf(stderr,"Selected map %d\n", mapType);
    			selection("set map", 1, base, fb);
    			display("main menu", base, fb);
    			exit = true;
	            break;
	        }
	        case 'a': //select map to the left
	        {
	            if (mapType == 1){
	                mapType = 3;
	            } else {
	                mapType -= 1;
	            }
	            printMaps(mapType, fb, base);
	            break;
	        }
	        case 'd': //select map to the right
	        {
	            if (mapType == 3){
	                mapType = 1;
	            } else {
	                mapType += 1;
	            }
	            printMaps(mapType, fb, base);
	            break;
	        }
	        default:
	            break;
	    }
	}while(!exit);
	
	fprintf(stderr, "------------------MAIN-MENU--------------------\n");
}

/************************************************************
This method opens up an opponent selection menu for the user
@set - settings struct
*************************************************************/ 
void aiSelect (settings* set, unsigned char* base, unsigned short* fb)
{
    fprintf(stderr, "-----------------AI-SELECTION-----------------\n");
	info("Select AI from E - M - A - H");
	fprintf(stderr, "'E' - EASY\n");
	fprintf(stderr, "'M' - MEDIUM\n");
	fprintf(stderr, "'A' - ADVANCED - TWO Easy AI\n");
	fprintf(stderr, "'H' - HARD - ONE Medium AI & ONE Easy AI\n");
	info("Press 'B' to get back to Main Menu");
	int ai;
	ai = getchar();
	switch(ai)
	{
		case 'e':
		case 'E':
		{
			set->ai = AI_EASY;
			setNotMedAi();
			info("Selected EASY AI");
			selection("set ai", 0, base, fb);
			display("main menu", base, fb);
			break;
		}
		case 'm':
		case 'M':
		{
			set->ai = AI_MEDIUM;
			setMedAi();
			info("Selected MEDIUM AI");
			selection("set ai", 1, base, fb);
			display("main menu", base, fb);
			break;
		}
		case 'a':
		case 'A':
		{
			set->ai = AI_ADVANCED;
			setNotMedAi();
			info("Selected ADVANCED - TWO Easy AI");
			selection("set ai", 2, base, fb);
			display("main menu", base, fb);
			break;
		}
		case 'h':
		case 'H':
		{
			set->ai = AI_HARD;
			setMedAi();
			info("Selected HARD - ONE Medium AI & ONE Easy AI");
			selection("set ai", 3, base, fb);
			display("main menu", base, fb);
			break;
		}
		case 'B': //back to main menu
		case 'b':
		{
		    selection("set ai", 4, base, fb);
		    display("main menu", base, fb);
		    break;
		}
		default:
		{
			info("WARN: Wrong key pressed!");
		    break;
		}
	}
	fprintf(stderr, "------------------MAIN-MENU--------------------\n");
}

/************************************************************
This method prints out game manual into terminal
*************************************************************/ 
void printMan(void)
{
	fprintf(stderr, "--------------------MANUAL--------------------\n");
	info("Press 'N' for start of New Game");
	info("Press 'P' for point selection");
	info("Press 'M' for map selection");
	info("Press 'A' for AI selection");
	info("Press 'H' for manual print");
	info("Press 'Q' for program quit");
	fprintf(stderr, "------------------MAIN-MENU--------------------\n");
}
//------------END_INPUT_C------------