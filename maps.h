/*******************************************************************
  File name:  maps.h
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/

#ifndef __MAPS_H__
#define __MAPS_H__

#include <stdbool.h>
#include "grid.h"
#include "input.h"

typedef struct
{
	int row;     //current y position
	int col;     //current x position
	int major;   //major colour
	int minor;   //minor colour
	int rowLast; //last y positon
	int colLast; //last x position
	int lastMove;
}locPlayer; //struct holding information about player or AI

/************************************************************
Function used to send points left on map to other files
************************************************************/
int sendPoints(void);

/************************************************************
Function used to get local representation of maps

@mapNum = number of map representation
@ret    = pointer to map array
************************************************************/
int *getMaps(int mapNum);

/************************************************************
Check presence of walls in surounding of player

@up, @last, @down, @left = values of directions
@map    = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
@player = struct holding information about player or AI
@ret    = Returns number of valid moves
************************************************************/
int checkValid(int* up, int* down, int* left, int* right, locPlayer* player, lineChunk* map);


/************************************************************
Selects and prints user chosen map in local representation

@line   = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
@map    = number of map that user selected
@player = struct holding information about player or AI
@ret    = Returns number of valid moves
************************************************************/
void initMap(unsigned short *fb, map_type map, lineChunk* line, locPlayer* player);

/************************************************************
Function used for generation and printing of points on map
Variable "safety" is used to limit infinite cycle (should not occur)

@fb     = array of RGB values of LCD display
@line   = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
@points = number of user chosed points
************************************************************/
void genPoints(unsigned short *fb, lineChunk* line, int points);

/************************************************************
Function used to print player or AI on map after move
If player (not AI) moves on tile with a point, point is collected

@fb       = array of RGB values of LCD display
@idx      = 2D array holding all indexes of current chunk
@chHeight = proportions of one chunk in y range
@chWidth  = proportions of one chunk in x range
@player   = struct holding information about player or AI
************************************************************/
void fillPlayer(unsigned short *fb, int** idx , int chHeight, int chWidth, locPlayer* player);

/************************************************************
Function used to clean points from local map repr. after game ends
************************************************************/
void cleanPoints(void);

/************************************************************
Function used check proximity of 2 turns around player for
presence of AI
@player = struct holding information about player 
@player = struct holding information about AI
@ret    = true, if there is AI close to player, false otherwise
************************************************************/
bool checkSquare(locPlayer* player, locPlayer* ai);

/************************************************************
---------------------Set of MOVE functions-------------------
Used to check validity of move and its commitment

@fb       = array of RGB values of LCD display
@map      = Initialized "Grid" struct - array, able to hold
            indexes of current "grid" line
@player   = struct holding information about player or AI
************************************************************/
bool moveRight(lineChunk* map, unsigned short* fb, locPlayer* player);

bool moveLeft(lineChunk* map, unsigned short* fb, locPlayer* player);

bool moveUp(lineChunk* map, unsigned short* fb, locPlayer* player);

bool moveDown(lineChunk* map, unsigned short* fb, locPlayer* player);
/************************************************************
------------------END of Set of MOVE functions---------------
************************************************************/

#endif
//------------END_MAPS_H------------
