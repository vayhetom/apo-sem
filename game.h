/*******************************************************************
  File name:  game.h
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/
#ifndef __GAME_H__
#define __GAME_H__ 

#include "grid.h"
#include "maps.h"
#include "input.h"

/************************************************************
Function dedicated to game maintenance
Setups game map + points
Reads from terminal for player movement

@set  = settings loaded in menu
@base = parlcd_mem_base adresses of display
@fb   = array of RGB values of display
*************************************************************/
void newGame(settings* set, unsigned char* base, unsigned short* fb);


/************************************************************
Check if there was a collision with AI 
(Swapped positions, or are on same)

@player = Struct holding information about 
          player (Position, lastPosition, colour)

@ai     = Struct holding information about 
          first AI (Position, lastPosition, colour)
          
@ai2    = Struct holding information about 
          second AI (Position, lastPosition, colour)
          
@return = True if there was a collision, false otherwise
************************************************************/
bool checkDeath(locPlayer* player, locPlayer* ai, locPlayer* ai2);


/************************************************************
Visualize Death screen (Hearth status)
@base   = parlcd_mem_base adresses of display

@fb     = array of RGB values of display
************************************************************/
void deathVis(unsigned short *fb, unsigned char* base);


/************************************************************
Subtract one life, check status of lives
Modifies global struct stats.lives

@return = true if player lost all his lives, false otherwise
************************************************************/
bool checkGameOver(void);


void printControls(void);

int sendLives(void);

int getOutcome(void);

locPlayer* sendPacMan(void);

#endif
//------------END_GAME_H------------