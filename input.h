/*******************************************************************
  File name:  input.h
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/

#ifndef __INPUT_H__
#define __INPUT_H__

typedef enum {
	MAP_FIRST,
	MAP_SECOND,
	MAP_THIRD,
}map_type;

typedef enum{
	AI_EASY,
	AI_MEDIUM,
	AI_ADVANCED,
	AI_HARD,
}ai_level;

typedef struct{
	map_type map;
	int points;
	ai_level ai;
}settings;


/**********************************************************
	Start of program logic - Menu 
	@base = parlcd_mem_base adresses for display
	@fb = pixel values for display
	
	base and fb are only forwarded towards other functions
***********************************************************/
void inputThr(unsigned char* base, unsigned short* fb);


/************************************************************
Utility method for game settings initialization
*************************************************************/ 
void settingsInit(settings* set);


/************************************************************
This method prints out game outcome
*************************************************************/ 
void finalScreen(unsigned char* base, unsigned short* fb);


/************************************************************
This method opens up a point selection menu for the user
@set - settings struct
*************************************************************/ 
void pointSelect(settings* set, unsigned char* base, unsigned short* fb);


/************************************************************
This method opens up a map selection menu for the user
@set - settings struct
*************************************************************/ 
void mapSelect(settings* set, unsigned char* base, unsigned short* fb);


/************************************************************
This method opens up an opponent selection menu for the user
@set - settings struct
*************************************************************/ 
void aiSelect (settings* set, unsigned char* base, unsigned short* fb);


/************************************************************
This method prints out game manual into terminal
*************************************************************/ 
void printMan(void);


/************************************************************
Utility method to send amount of points to other files
Retruns number of points
*************************************************************/ 
int getPts(void);

#endif
