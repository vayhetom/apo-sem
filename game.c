/*******************************************************************
  File name:  game.c
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/

#include <stdio.h>
#include <pthread.h>

#include "utils.h"
#include "game.h"
#include "maps.h"
#include "input.h"
#include "grid.h"
#include "gameAi.h"
#include "ledLine.h"
#include "menuFunc.h"

int checkProximity(locPlayer* player, locPlayer* ai, locPlayer* ai2);

bool evalvMove(settings* set, unsigned char* base, unsigned short* fb, 
				locPlayer* player, lineChunk* line);

typedef struct{
	int lives;
	int points;
}playerStats;

playerStats stats = {.lives = 3}; //Global struct holding player lives and points

locPlayer player = {.row = 0, .col = 0, .major = 0, .minor = 0, .rowLast = 0, .colLast = 0}; //Global struct holding player position

static int outcome; //"Global" variable - Information about cause of Game Over

/************************************************************
Function dedicated to game maintenance
Setups game map + points
Reads from terminal for player movement

@set  = settings loaded in menu
@base = parlcd_mem_base adresses of display
@fb   = array of RGB values of display
*************************************************************/
void newGame(settings* set, unsigned char* base, unsigned short* fb)
{
    stats.lives = 3;
    outcome = 2;
	eraseBuffer(fb);
	turnOfLed();
	lineChunk* line = gridInit(32,32);
	initMap(fb, set->map, line, &player);
	genPoints(fb, line, set->points);
	redrawDisp(fb, base);
	printControls();
	stats.points = sendPoints(); //Initialise local representation of remaining points
	usleep(100 * 1000); // 100 ms
	char move;
	setMove();
	while((move =getchar())!= 'q') //Commit player moves
	{
		bool valid = false;
		if(move == 'Q')
		{	
			break; //init quit game
		}
		switch(move)
		{
			case 'W':
			case 'w':
			{
				info("Moving up");
				if(moveUp(line, fb, &player))
				{
					warn("You cant move up into wall!");
					break;
				}
				valid = true;
				break;
			}
			case 'A':
			case 'a':
			{
				info("Moving left");
				if(moveLeft(line, fb, &player))
				{
					warn("You cant move left into wall!");
					break;
				}
				valid = true;
				break;
			}
			case 'S':
			case 's':
			{
				info("Moving down");
				if(moveDown(line, fb, &player))
				{
					warn("You cant move down into wall!");
					break;
				}
				valid = true;
				break;
			}
			case 'D':
			case 'd':
			{
				info("Moving right");
				if(moveRight(line, fb, &player))
				{
					warn("You cant move right into wall!");
					break;
				}
				valid = true;
				break;
			}
			default:
			{
				warn("Calm down! Wrong key pressed");
				break;
			}
		}
		if(valid) //Move vas performed
		{
			if(evalvMove(set, base, fb, &player, line)) //Evaluate status of game after move
			{
				break; //Game over player died or won
			}
		}
	}
	setQuit(); //Exit all threads
	setNotMove(); //Turn of AI
	cleanPoints(); //Erase points representation in maps
	turnOfLed();
	eraseBuffer(fb);
	redrawDisp(fb, base);
	gridFree(line);
}

/************************************************************
Function dedicated to evaluation after each valid move
Sends information into RGB thread and ledline
Edits global variables (stats and outcome)
@set    = settings loaded in menu
@base   = parlcd_mem_base adresses of display

@fb     = array of RGB values of display

@player = Struct holding information about 
          player (Position, lastPosition, colour)
          
@line   = Initialised "Grid" struct - array, able to hold
          indexes of current "grid" line
          
@return = True if occured Game Over - Player won (collecte
          all points) or player lost (collided with AI)
          False otherwise
************************************************************/
bool evalvMove(settings* set, unsigned char* base, unsigned short* fb, 
				locPlayer* player, lineChunk* line)
{
	bool ret = false;
	
	while(isMove() || isMove2())   //wait for AI
	{
		if(isQuit())
		{
			break;
		}
		usleep(1000); // 1 ms
	}
	redrawDisp(fb, base);
	visualizePts(set->points);
	locPlayer* repAi = sendPlayer(); //image of AI player
	locPlayer* repAi2 = (set->ai == AI_ADVANCED || set->ai == AI_HARD) ? sendPlayer2() : NULL; 
	if(checkDeath(player, repAi, repAi2))
	{
		info("Oh no, You Died!");
		if (stats.lives != 0 && stats.lives != 1){
		    deathVis(fb, base);
		}
		if(checkGameOver()) //Collided with AI
		{
			//ending screen
			info("GAME OVER");
			outcome = 0;
			ret = true;
			return ret;
		}
		initAi(fb, line);
		if(repAi2)
		{
			initSecond(fb, line);
		}
		initMap(fb, set->map, line, player);	
		redrawDisp(fb, base);			
	}
	if(sendPoints() == 0) //All points are collected
	{
		//Game over, you WON!
		info("YOU WON!");
		outcome = 1;
		ret = true;
		return ret;
	}
	int options = 1;
	bool collected = false;
	int pts = sendPoints();
	if(pts != stats.points) //Point was collected
	{	
	    collected = true;
		stats.points = pts;
		setRGB(options);
	}
	int prox = checkProximity(player, repAi, repAi2);
	if(prox) //AI is in proximity of 2 turns
	{
		setRGB(prox);
	}
	if(!prox && !collected)
	{
		setNotRGB();
	}
	setMove();
	return ret;
}

/************************************************************
Check if there is AI in vect. distance of 2 (4 as squared dist)

@player = Struct holding information about 
          player (Position, lastPosition, colour)

@ai     = Struct holding information about 
          first AI (Position, lastPosition, colour)
          
@ai2    = Struct holding information about 
          second AI (Position, lastPosition, colour)
          
@return = 0 if there is no AI in range of 2 turns
        = 2 or 3 or 4 in case ther is AI (value represents settings for RGB period)
************************************************************/
int checkProximity(locPlayer* player, locPlayer* ai, locPlayer* ai2)
{
	int ret = 0;
	int dist1 = calcDist(player, ai->row, ai->col);
	if(dist1 == 1) //Square of vector distance - Neighbording tiles
	{
		ret = 2;
	}
	if(dist1 == 2) //Square of vector distance - closest diagonal tiles
	{
		ret = 3;
	}
	if(dist1 == 4) //Square of vector distance - 2 tiles in basic directions (N/W/S/E)
	{
		if(checkSquare(player, ai))
		{
			ret = 4;
		}
	}
	if(ai2 && (ret == 0 || ret > 2)) //Check same for second ai (if there is any)
	{
		int dist2 = calcDist(player, ai2->row, ai2->col);
		if(dist2 == 1)
		{
			ret = 2;
		}
		if(dist2 == 2 && (ret == 0 || ret > 3))
		{
			ret = 3;
		}
		if(dist2 == 4 && ret == 0)
		{
			if(checkSquare(player, ai2))
			{
				ret = 4;
			}
		}
	}
	return ret;
}

/************************************************************
Visualize Death screen (Hearth status)
@base   = parlcd_mem_base adresses of display

@fb     = array of RGB values of display
************************************************************/
void deathVis(unsigned short *fb, unsigned char* base)
{
	bool center = true;
	int color = 0x8800;
	
	eraseBuffer(fb);
	printString(sendHeartsPlain(), 2*16, fb, color, center);
	redrawDisp(fb, base);
	eraseBuffer(fb);
	usleep(1000000); // 1s
	for(int i = 0; i < 320*480; ++i)
	{
		fb[i] = 0x0000;
	}
	redrawDisp(fb, base);
	eraseBuffer(fb);
}

/************************************************************
Check if there was a collision with AI 
(Swapped positions, or are on same)

@player = Struct holding information about 
          player (Position, lastPosition, colour)

@ai     = Struct holding information about 
          first AI (Position, lastPosition, colour)
          
@ai2    = Struct holding information about 
          second AI (Position, lastPosition, colour)
          
@return = True if there was a collision, false otherwise
************************************************************/
bool checkDeath(locPlayer* player, locPlayer* ai, locPlayer* ai2)
{
	bool over = false;
	if(player->col == ai->col && player->row == ai->row) //Same Position
	{
		over = true;
	}
	if(player->col == ai->colLast && player->row == ai->rowLast) //May have swapped positions
	{
		if(ai->col == player->colLast && ai->row == player->rowLast) //swapped positions
		{
			over = true;
		}
	}
	if(ai2) //Two AI game
	{
		if(player->col == ai2->col && player->row == ai2->row) //Same Position
		{
			over = true;
		}
		if(player->col == ai2->colLast && player->row == ai2->rowLast) //May have swapped positions
		{
			if(ai2->col == player->colLast && ai2->row == player->rowLast) //swapped positions
			{
				over = true;
			}
	}
	}
	return over;
}



void printControls(void)
{
	fprintf(stderr, "-------------------CONTROLS--------------------\n");
	info("Press 'w' for moving up");
	info("Press 's' for moving down");
	info("Press 'a' for moving left");
	info("Press 'd' for moving right");
	fprintf(stderr, "------------------END_CONTROLS-----------------\n");
}
/************************************************************
Subtract one life, check status of lives
Modifies global struct stats.lives

@return = true if player lost all his lives, false otherwise
************************************************************/
bool checkGameOver(void)
{
	--stats.lives;
	if(stats.lives == 0)
	{
		return true;
	}
	return false;
}

int sendLives(void)
{
	return stats.lives;
}

int getOutcome(void){
    return outcome;
}

locPlayer* sendPacMan(void)
{
	locPlayer* pac = &player;
	return pac;
}
//------------END_GAME_C------------