/*******************************************************************
  File name:  gameAi.c
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include "maps.h"
#include "utils.h"
#include "grid.h"
#include "gameAi.h"
#include "game.h"

int moveAiEasy(unsigned short *fb, lineChunk* line, int last, locPlayer* ai);

char moveMediumAi(unsigned short *fb, lineChunk* line, int last, locPlayer* ai);

int calcDist(locPlayer* pacMan, int aiRow, int aiCol);

char findMoveValue(int up, int down, int left, int right, locPlayer* ai);

void commitLast(unsigned short *fb, lineChunk* line, int *last, locPlayer* ai);

void commitNext(unsigned short *fb, lineChunk* line, int up, int down,
				 int left, int right, int *last, locPlayer* ai);

//Local representation of AI "players". Position, last position and colour
locPlayer aiFirst = {.row = 0, .col =0, .major = 0, .minor = 0, .rowLast =0, .colLast = 0};
locPlayer aiSecond = {.row = 0, .col =0, .major = 0, .minor = 0, .rowLast =0, .colLast = 0};

//Struct for synchonization
static struct
{
	bool quit;  //signal to end all threads associated with game
	bool move;  //"moving" first ai
	bool move2; //"moving" second ai
	bool init;  //Dont have to init mutexes again
	bool medAi; //Flag for use of medium AI dif
	pthread_mutex_t mtx;
	pthread_mutex_t aiContr;
	pthread_cond_t cond;
}locAi = {.quit = false, .move = false, .init = false, .move2 = false, .medAi = false}; 

/************************************************************
Function used to init mutexes and local "aiFirst" representation 
to starting position and send signal to draw AI "player"

@fb   = array of RGB values of display

@map  = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
************************************************************/
void initAi(unsigned short *fb, lineChunk* map)
{
	gridIndex(map,1);
	oneChunk *chunk = map->chunks;
	if(!locAi.init)
	{
		locAi.init = true;
		pthread_mutex_init(&(locAi.mtx), NULL);
		pthread_mutex_init(&(locAi.aiContr), NULL);
		pthread_cond_init(&(locAi.cond), NULL);
	}
	int** idx = chunk[1].index; //TOP LEFT CORNER
	aiFirst.row = 1;
	aiFirst.col = 1;
	aiFirst.rowLast = 1;
	aiFirst.colLast = 1;
	aiFirst.major = 0xe800;
	aiFirst.minor = 0xf360;
	aiFirst.lastMove = 'd';
	fillPlayer(fb, idx , map->chHeight, map->chWidth, &aiFirst);
}

/************************************************************
Function used to local "aiSecond" representation to starting
position and send signal to draw AI "player"

@fb   = array of RGB values of display

@map  = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
************************************************************/
void initSecond(unsigned short *fb, lineChunk* map)
{
	gridIndex(map,8);
	oneChunk *chunk = map->chunks;
	int** idx = chunk[13].index; //TOP LEFT CORNER
	aiSecond.row = 8;
	aiSecond.col = 13;
	aiSecond.rowLast = 8;
	aiSecond.colLast = 13;
	aiSecond.major = 0xf8e0;
	aiSecond.minor = 0xf360;
	aiSecond.lastMove = 'd';
	fillPlayer(fb, idx , map->chHeight, map->chWidth, &aiSecond);
}

/************************************************************
Function used as a thread for "aiSecond" 
movement and synchronization

@arg  = array of RGB values of display

Thread is detached before return
************************************************************/
void* logicSecondAi(void* arg)
{
	unsigned short* fb = (unsigned short*) arg;
	lineChunk* line = gridInit(32,32);
	info("START AI 2");
	pthread_mutex_lock(&(locAi.aiContr));
	initSecond(fb, line);
	pthread_mutex_unlock(&(locAi.aiContr));
	bool quit = isQuit();
	while(!quit)
	{
		while(!isMove())
		{
			if(isQuit())
			{
				break;
			}
		}
		pthread_mutex_lock(&(locAi.mtx));
		locAi.move2 = true;
		pthread_mutex_unlock(&(locAi.mtx));
		info("Moving second AI!");
		pthread_mutex_lock(&(locAi.aiContr));
		aiSecond.lastMove = moveAiEasy(fb, line, aiSecond.lastMove, &aiSecond);
		locAi.move2 = false;
		pthread_mutex_unlock(&(locAi.aiContr));
		quit = isQuit();
	}
	pthread_detach(pthread_self()); 
	gridFree(line);
	return NULL;
}

/************************************************************
Function used as a thread for "aiFirst" 
movement and synchronization

@arg  = array of RGB values of display

Thread is detached before return
************************************************************/
void* logicAi(void* arg)
{
	unsigned short* fb = (unsigned short*) arg;
	lineChunk* line = gridInit(32,32);
	info("START AI 1");
	pthread_mutex_lock(&(locAi.aiContr));
	initAi(fb, line);
	pthread_mutex_unlock(&(locAi.aiContr));
	bool quit = isQuit();
	while(!quit)
	{
		while(!isMove())
		{
			if(isQuit())
			{
				break;
			}
		}
		info("Moving AI!");
		pthread_mutex_lock(&(locAi.aiContr));
		if(locAi.medAi)
		{
			aiFirst.lastMove = moveMediumAi(fb, line, aiFirst.lastMove, &aiFirst);
		}
		else
		{
			aiFirst.lastMove = moveAiEasy(fb, line, aiFirst.lastMove, &aiFirst);
		}
		pthread_mutex_unlock(&(locAi.aiContr));
		quit = isQuit();
	}
	pthread_detach(pthread_self()); 
	gridFree(line);
	return NULL;
}

/************************************************************
AI Moves Randomly with memmory of last move

@fb   = array of RGB values of display

@line = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
@last = Last move of AI
@ai   = Struct holding information about current AI
@ret  = Returns commited move
************************************************************/
int moveAiEasy(unsigned short *fb, lineChunk* line, int last, locPlayer* ai)
{
	bool valid = false;
	int up = 0;
	int down = 1;
	int left = 2;
	int right = 3;
	int range = checkValid(&up, &down, &left, &right, ai, line);
	int move = -5;
	while(!valid)
	{
		if(isQuit())
		{
			break;
		}
		if(range == 2)
		{
			commitNext(fb, line, up, down, left, right, &last, ai);
			move = last;
			break;
		}
		srand(time(0)); //seed for random gen	
		move = range > 1 ? (rand() % range) : 0; //numbers 0 - 4 (as 4 directions)
		if(move == up)
		{
			if(last == 'd' && range > 1)
			{
				continue;
			}
			if(moveUp(line, fb, ai))
			{
				continue;
			}
			valid = true;
			move = 'u';
		}
		if(move == down)
		{
			if(last == 'u' && range > 1)
			{
				continue;
			}
			if(moveDown(line, fb, ai))
			{
				continue;
			}
			valid = true;
			move = 'd';
		}
		if(move == left)
		{
			if(last == 'r' && range > 1)
			{
				continue;
			}
			if(moveLeft(line, fb, ai))
			{
				continue;
			}
			valid = true;
			move = 'l';
		}
		if(move == right)
		{
			if(last == 'l' && range > 1)
			{
				continue;
			}
			if(moveRight(line, fb, ai))
			{
				continue;
			}
			valid = true;
			move = 'r';
		}
	} //END WHILE
	setNotMove();
	return move;
}

/************************************************************
Returns ai player to his last position

@line = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
@last = Last move of AI
@ai   = Struct holding information about current AI
@ret  = Returns commited move
************************************************************/
void commitLast(unsigned short *fb, lineChunk* line, int *last, locPlayer* ai)
{
	if(*last == 'u')
	{
		moveDown(line, fb, ai);
		*last = 'd';
	}
	if(*last == 'd')
	{
		moveUp(line, fb, ai);
		*last = 'u';
	}
	if(*last == 'l')
	{
		moveRight(line, fb, ai);
		*last = 'r';
	}
	if(*last == 'r')
	{
		moveLeft(line, fb, ai);
		*last = 'l';
	}
}

/************************************************************
AI chooses next move based on closest vector distance between
AI and player

@fb   = array of RGB values of display

@line = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
@last = Last move of AI
@ai   = Struct holding information about current AI
@up, @last, @down, @left = values of directions
@ret  = Returns commited move
************************************************************/
void commitNext(unsigned short *fb, lineChunk* line, int up, int down,
				 int left, int right, int *last, locPlayer* ai)
{
	if(up >=0 && *last!= 'd')
	{
		moveUp(line, fb, ai);
		*last = 'u';
		return;
	}
	if(down >=0 && *last!= 'u')
	{
		moveDown(line, fb, ai);
		*last = 'd';
		return;
	}
	if(left >=0 && *last!= 'r')
	{
		moveLeft(line, fb, ai);
		*last = 'l';
		return;
	}
	if(right >=0 && *last!= 'l')
	{
		moveRight(line, fb, ai);
		*last = 'r';
		return;
	}
}
/************************************************************
AI chooses next move based on closest vector distance between
AI and player

@fb   = array of RGB values of display

@line = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
@last = Last move of AI
@ai   = Struct holding information about current AI
@ret  = Returns commited move
************************************************************/
char moveMediumAi(unsigned short *fb, lineChunk* line, int last, locPlayer* ai)
{
	int up = 0;
	int down = 1;
	int left = 2;
	int right = 3;
	checkValid(&up, &down, &left, &right, ai, line);
	char move = findMoveValue(up, down, left, right, ai);
	switch(move)
	{
		case 'x': //Nothing is better than current position, return to last tile
		{
			commitLast(fb, line, &last, ai);
			move = last;
			break;
		}
		case 'l': //left
		{
			moveLeft(line, fb, ai);
			break;
		}
		case 'r': //right
		{
			moveRight(line, fb, ai);
			break;
		}
		case 'd': //down
		{
			moveDown(line, fb, ai);
			break;
		}
		case 'u': //up
		{
			moveUp(line, fb, ai);
			break;
		}
	}
	setNotMove();
	return move;
}

/************************************************************
Find move with lowest distance between ai and player

@ai   = Struct holding information about current AI
@up, @last, @down, @left = values of directions
@ret  = Returns best move
************************************************************/
char findMoveValue(int up, int down, int left, int right, locPlayer* ai)
{	
	locPlayer* pacMan = sendPacMan();
	int min = calcDist(pacMan, ai->row, ai->col);
	char dir = 'x';
	if(up >= 0)
	{
		int ref = calcDist(pacMan, ai->row - 1, ai->col);
		if(ref <= min)
		{
			min = ref;
			dir = 'u';
		}
	}
	if(down >= 0)
	{
		int ref = calcDist(pacMan, ai->row + 1, ai->col);
		if(ref <= min)
		{
			min = ref;
			dir = 'd';
		}
	}
	if(left >= 0)
	{
		int ref = calcDist(pacMan, ai->row, ai->col - 1);
		if(ref <= min)
		{
			min = ref;
			dir = 'l';
		}
	}
	if(right >= 0)
	{
		int ref = calcDist(pacMan, ai->row, ai->col + 1);
		if(ref <= min)
		{
			min = ref;
			dir = 'r';
		}
	}
	return dir;
}

/************************************************************
@pacMan = struct holding information abou player
@aiRow  = Y position of AI on the board
@aiCol  = X position of AI on the board
@ret    = returns squared vector distance between player and AI
************************************************************/
int calcDist(locPlayer* pacMan, int aiRow, int aiCol)
{
	int vectY = pacMan->row - aiRow;
	int vectX = pacMan->col - aiCol;
	int dist = vectX * vectX + vectY * vectY; //squared dist
	return dist;
}

/************************************************************
 -----------------SYNCHRONIZATION_BLOCK----------------------
 ************************************************************/
void setNotQuit(void)
{
	pthread_mutex_lock(&(locAi.mtx));
	locAi.quit = false;
	pthread_mutex_unlock(&(locAi.mtx));
}
void setQuit(void)
{
	pthread_mutex_lock(&(locAi.mtx));
	locAi.quit = true;
	pthread_mutex_unlock(&(locAi.mtx));
}

bool isQuit(void)
{
	pthread_mutex_lock(&(locAi.mtx));
	bool ret = locAi.quit;
	pthread_mutex_unlock(&(locAi.mtx));
	return ret;
}
void setNotMove(void)
{
	pthread_mutex_lock(&(locAi.mtx));
	locAi.move = false;
	pthread_mutex_unlock(&(locAi.mtx));
}
void setMove(void)
{
	pthread_mutex_lock(&(locAi.mtx));
	locAi.move = true;
	pthread_mutex_unlock(&(locAi.mtx));
}

bool isMove(void)
{
	pthread_mutex_lock(&(locAi.mtx));
	bool ret = locAi.move;
	pthread_mutex_unlock(&(locAi.mtx));
	return ret;
}

bool isMove2(void)
{
	pthread_mutex_lock(&(locAi.mtx));
	bool ret = locAi.move2;
	pthread_mutex_unlock(&(locAi.mtx));
	return ret;
}

void setMedAi(void)
{
	locAi.medAi = true;
}

void setNotMedAi(void)
{
	locAi.medAi = false;
}
locPlayer* sendPlayer(void)
{
	locPlayer* ai = &aiFirst;
	return ai;
}

locPlayer* sendPlayer2(void)
{
	locPlayer* ai = &aiSecond;
	return ai;
}
/************************************************************
 ----------------END_SYNCHRONIZATION_BLOCK-------------------
 ************************************************************/
//------------END_GAME_AI_C------------