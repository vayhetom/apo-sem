/*******************************************************************
  File name:  grid.c
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "grid.h"

#define DISP_XLEN 480
#define DISP_YLEN 320

/************************************************************
Function used to init struct able to hold
indexes of current "grid" line

@width   = proportions of one chunk in x range
@height  = proportions of one chunk in y range
************************************************************/

lineChunk* gridInit(int width, int height)
{
    if(DISP_XLEN % width != 0 || DISP_YLEN % height != 0)
    {
        fprintf(stderr,"ERROR: Wrong chunk proportions!");
        exit(100);
    }
    lineChunk* map = malloc(sizeof(lineChunk));
    map->xLen = DISP_XLEN / width;
    map->yLen = DISP_YLEN / height;    
    map->chWidth = width;
    map->chHeight = height;    
    oneChunk* chunk = malloc(map->xLen * sizeof(oneChunk)); //one line of chunks
    for(int i = 0; i < map->xLen; ++i)
    {
    	int** idx = malloc(height * sizeof(int*));
    	for(int j = 0; j < height; j++)
    	{
    		idx[j] = malloc(width * sizeof(int));
    	}
    	chunk[i].index = idx;
    }
    map->chunks = chunk;
    return map;
}

/************************************************************
Function used to fill "map" with display indexes in current "line"

@line = row of grid (display cut to chunks)
@map  = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
************************************************************/

void gridIndex(lineChunk* map, int line)
{
	int offset = line* map->chWidth *DISP_XLEN;
	int step = offset;
	oneChunk* chunk = map->chunks;
    for (int i = 0; i < map->chHeight; i++)
    {
        int col = 0;
        for (int j = 0; j < 480; j++)
        {
        	if(j % map->chWidth == 0 && j != 0)
        	{
        		//next chunk in line
        		++col;
        	}
        	
            chunk[col].index[i][j % map->chWidth] = step++;
        }    
    }
}

/************************************************************
Function used to free all dynamically allocated memmory
associated with "map"

@map  = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
************************************************************/

void gridFree(lineChunk* map)
{
	oneChunk *chunk = map->chunks;
    for(int i = 0; i < map->xLen; ++i)
    {
    	
    	for(int j = 0; j < map->chHeight; j++)
    	{
    		free(chunk[i].index[j]);
    	}
    	free(chunk[i].index);
    }
    free(chunk);
    free(map);
}

