/*******************************************************************
  File name:  menuFunc.c
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  28.5.2020
 *******************************************************************/

#include <string.h>
#include <unistd.h>
#include <stdbool.h>

#include "font_types.h"
#include "grid.h"
#include "menuFunc.h"
#include "utils.h"
#include "maps.h"
#include "game.h"
#include "input.h"

#define MAX_SYMBOLS 12
#define MAX_LINES 5
#define CHUNK_SIZE_S 4
#define CHUNK_SIZE_L 16
#define MAP_WIDTH 15
#define LETTER_HEIGHT 16
#define LETTER_WIDTH 10

/**********************************************************
	This file holds major methods for text and map printing 
	@base = parlcd_mem_base adresses for display
	@fb = pixel values for display
***********************************************************/

/* Map of letters
 * A = 10, B = 11, C = 12, D = 13, E = 14, F = 15, G = 16, H = 17, I = 18, J = 19,
 * K = 20, L = 21, M = 22, N = 23, O = 24, P = 25, Q = 26, R = 27, S = 28, T = 29,
 * U = 30, V = 31, W = 32, X = 33, Y = 34, Z = 35, ' ' = 36, '-' = 37, '!' = 38,
 * '<' = 39, '>' = 40, heart = 41, '+' = 42
 */
 
//2D arrays containing encoded text
static int mainMenu[MAX_LINES][MAX_SYMBOLS] = 
{
    {36,23,37,23,14,32,36,16,10,22,14,36},
    {36,22,37,28,14,29,36,22,10,25,36,36},
    {36,36,10,37,28,14,29,36,10,18,36,36},
    {25,37,28,14,29,36,25,24,18,23,29,28},
    {36,36,36,26,37,26,30,18,29,36,36,36}
};

static int setMap[MAX_LINES][MAX_SYMBOLS] = 
{
    {36,39,36,36,36,36,36,36,36,36,40,36},
    {36,36,28,37,28,14,21,14,12,29,36,36},
    {36,36,36,11,37,11,10,12,20,36,36,36},
};

static int setAi[MAX_LINES][MAX_SYMBOLS] = 
{
    {36,36,36,14,37,14,10,28,34,36,36,36},
    {36,36,22,37,22,14,13,18,30,22,36,36},
    {36,10,37,10,13,31,10,23,12,14,13,36},
    {36,36,36,17,37,17,10,27,13,36,36,36},
    {36,36,36,11,37,11,10,12,20,36,36,36}
};

static int setPts[MAX_LINES][MAX_SYMBOLS] = 
{
    {36,36,36,36,36,36,36,36,36,36,36,36},
    {36,36,36,36,36,36,36,36,36,36,36,36},
    {36,36,36,37,36, 2, 5,36,42,36,36,36},
    {36,36,36,36,36,36,36,36,36,36,36,36},
    {36,36,36,11,37,11,10,12,20,36,36,36}
};

int hearts[MAX_LINES][MAX_SYMBOLS] =
{
    {36,36,36,36,36,36,36,36,36,36,36,41},
    {36,36,36,36,36,36,36,36,36,36,41,41},
    {36,36,36,36,36,36,36,36,36,41,41,41}
};

int heartsPlain[MAX_LINES][MAX_SYMBOLS] =
{
    {36,36,36,36,36,36,41,36,36,36,36,36},
    {36,36,36,36,36,41,41,36,36,36,36,36},
    {36,36,36,36,41,41,41,36,36,36,36,36}
};

int gameOver[MAX_LINES][MAX_SYMBOLS] =
{
    {36,36,36,36,36,36,36,36,36,36,36,36},
    {36,16,10,22,14,36,24,31,14,27,38,36},
    {36,36,36,36,36,36,36,36,36,36,36,36},
    {36,36,36,36,36,36,36,36,36,36,36,36},
    {36,36,36,11,37,11,10,12,20,36,36,36}
};

int youWon[MAX_LINES][MAX_SYMBOLS] =
{
    {36,36,36,36,36,36,36,36,36,36,36,36},
    {36,36,34,24,30,36,32,24,23,38,36,36},
    {36,36,36,36,36,36,36,36,36,36,36,36},
    {36,36,36,36,36,36,36,36,36,36,36,36},
    {36,36,36,11,37,11,10,12,20,36,36,36}
};

lineChunk* map1; //grid for letters

lineChunk* map2; //grid for maps

void mapInit1(void)
{
    map1 = gridInit(CHUNK_SIZE_S, CHUNK_SIZE_S);
}


void mapInit2(void)
{
    map2 = gridInit(CHUNK_SIZE_L, CHUNK_SIZE_L);
}


void mapFree1(void)
{
    if(map1)
    {
        gridFree(map1);
    }
}


void mapFree2(void)
{
    if(map2)
    {
        gridFree(map2);
    }
}

/************************************************************
Uses information about lives from game.c
Returns pointer to array with encoded symbols 
*************************************************************/ 
int *sendHearts(void)
{
    int *heartsPtr = NULL;
    if (sendLives() == 1)
    {
        heartsPtr = hearts[0];
    } 
    else if (sendLives() == 2)
    {
        heartsPtr = hearts[1];
    } 
    else if (sendLives() == 3)
    {
        heartsPtr = hearts[2];
    }
    return heartsPtr;
}


int *sendHeartsPlain(void)
{
    int *heartsPtr = NULL;
    if (sendLives() == 2)
    {
        heartsPtr = heartsPlain[0];
    } 
    else if (sendLives() == 3)
    {
        heartsPtr = heartsPlain[1];
    }
    return heartsPtr;
}

/************************************************************
Uses information about points from input.c
Returns pointer to array with digits of point amount
*************************************************************/ 
int *importPts(void)
{
    int pts = getPts();
    static int ret[2] = {0};
    int counter = 0;
    
    if (pts % 10 == 5)
    {
        ret[1] = 5;
    } 
    else 
    {
        ret[1] = 0;
    }
   
    while (pts >= 10){
        pts -= 10;
        ++counter;
    }
    ret[0] = counter;
    return ret;
}

/************************************************************
Calls function printString() based on @type, which is string
that says what to display.
*************************************************************/ 
void display(char *type, unsigned char* base, unsigned short* fb){
    
    eraseBuffer(fb); //clear display
    bool center = false; //boolean for centering a text
    int color = 0x3333;
    
    if (strcmp(type,"main menu") == 0)
    {
        for (int i = 0; i < MAX_LINES; i++)
        {
            if (i == 1)
            {
                center = true;
            }
            else
            {
                center = false;
            }
            printString(mainMenu[i], LETTER_HEIGHT*i, fb, color, center);
        }
    } 
    else if (strcmp(type,"set ai") == 0)
    {
        for (int i = 0; i < MAX_LINES; i++)
        {
            printString(setAi[i], LETTER_HEIGHT*i, fb, color, center);
        }
    } 
    else if (strcmp(type,"set points") == 0)
    {
        for (int i = 0; i < MAX_LINES; i++)
        {
            printString(setPts[i], LETTER_HEIGHT*i, fb, color, center);
        }
    } 
    else if (strcmp(type,"points change") == 0)
    {
        int *value = importPts();
        setPts[2][5] = value[0];
        setPts[2][6] = value[1];
        for (int i = 0; i < MAX_LINES; i++)
        {
            printString(setPts[i], LETTER_HEIGHT*i, fb, color, center);
        }
    } 
    else if (strcmp(type,"you won") == 0)
    {
        for (int i = 0; i < MAX_LINES; i++)
        {
            printString(youWon[i], LETTER_HEIGHT*i, fb, color, center);
        }
    } 
    else if (strcmp(type,"game over") == 0)
    {
        for (int i = 0; i < MAX_LINES; i++)
        {
            printString(gameOver[i], LETTER_HEIGHT*i, fb, color, center);
        }
    }
    
    redrawDisp(fb, base); //write data on display
}

/************************************************************
Prints characters on display

@stringOffset - particular 1D array in 2D array
@chunkOffset - row on display
@center - information about centering the text if theres an
odd number of symbols in it
*************************************************************/
void printString(int *stringOffset, int chunkOffset, unsigned short* fb, int color, bool center)
{
    unsigned int height = pac_man_font.height;
    int width = pac_man_font.maxwidth;
    int maxLetter = MAX_SYMBOLS;
    int pixelString[maxLetter][height][width];

    //multiply offsets by 16
    for (int i = 0; i < maxLetter; i++)
    {
        stringOffset[i] *= LETTER_HEIGHT; 
    }

    //load a letter string row by row, maximum of letters is 12
    for (int k = 0; k < maxLetter; ++k)
    {
        //init letter array with zeros
        for (int i = 0; i < height; i++){
            for (int j = 0; j < width; j++)
            {
                pixelString[k][i][j] = 0;
            }
        }

        //write font vaules to array
        for (int i = 0; i < height; i++)
        {
            if (pac_man_font.bits[stringOffset[k]+i] > 0)
            {
                convert(pac_man_font.bits[stringOffset[k]+i], pixelString[k][i], width, height);
            }
        }
    }

    int ptr = 0;
    int x = 0;
    int i2= 0;
    int widthAlt;
    
    //print the string
    for (int i = 0; i < CHUNK_SIZE_S*height; i++) //move vertically
    { 
        i2 = i/CHUNK_SIZE_S;
        oneChunk *chunk = map1->chunks;
        gridIndex(map1, chunkOffset + i2);
        for (int k = 0; k < maxLetter; k++) //move letter by letter
        { 
            //flag - cut 5 chunks from right
            if (center && k == maxLetter - 1)
            {
                widthAlt = width/2;
            }
            else
            {
                widthAlt = width;
            }
            
            for (int j = 0; j < widthAlt; j++) //move horizontally
            { 
                if (pixelString[k][i2][j] == 1) //if 4x4 pixel chunk is lit
                { 
                    for (int y = 0; y < CHUNK_SIZE_S; y++) //move on chunk width index
                    { 
                        x = i % CHUNK_SIZE_S; //move on chunk height index
                        //if center is true - add 5 chunks from left
                        if (center)
                        {
                            ptr = chunk[width/2+j+k*LETTER_WIDTH].index[x][y];
                        }
                        else
                        {
                            ptr = chunk[j+k*LETTER_WIDTH].index[x][y];
                        }
                        
                        fb[ptr] = color;
                    }    
                }
            }
        }
    }
    
    //divide offsets by 16
    for (int i = 0; i < maxLetter; i++)
    {
        stringOffset[i] /= LETTER_HEIGHT; 
    }
}

/************************************************************
Calls function blink() based on @type, which is string
that says what to display.

@option - particular 1D array in 2D array
*************************************************************/ 
void selection(char *type, int option, unsigned char* base, unsigned short* fb){
    bool center = false; //boolean for centering a text
    
    if (strcmp(type,"main menu") == 0)
    {
        if (option == 1){
            center = true;
        } 
        else 
        {
            center = false;
        }
        blink(mainMenu[option], LETTER_HEIGHT*option, fb, base, center);
    } 
    else if (strcmp(type,"set map") == 0)
    {
        blink(setMap[option], LETTER_HEIGHT*2+LETTER_HEIGHT*option, fb, base, center);
    } 
    else if (strcmp(type,"set ai") == 0)
    {
        blink(setAi[option], LETTER_HEIGHT*option, fb, base, center);
    } 
    else if (strcmp(type,"set points") == 0)
    {
        blink(setPts[option], LETTER_HEIGHT*option, fb, base, center);
    }
}

/************************************************************
Prints representation of maps

@mapNum - array of encoded map
*************************************************************/
void printMaps(int mapNum, unsigned short* fb, unsigned char* base)
{
    eraseBuffer(fb); //clear display
    
    int j = 0; 
    int ptr = 0;
    int offset1 = 1;
    int offset2 = 6;
    int *mapField = getMaps(mapNum);
    
    for (int i = 0; i < MAP_WIDTH*LETTER_WIDTH; i++) //move horizonatlly through map array
    { 
        oneChunk *chunk = map2->chunks;
        if (i % MAP_WIDTH == 0){
            gridIndex(map2, offset1 + i/MAP_WIDTH);
            j = 0;
        }
        j++;
        if (mapField[i] == 1) //if 4x4 pixel chunk is lit
        { 
            for (int y = 0; y < CHUNK_SIZE_L; y++) //move on chunk width index
            { 
                for (int x = 0; x < CHUNK_SIZE_L; x++) //move on chunk height index
                {
                    ptr = chunk[offset2+j].index[x][y];
                    fb[ptr] = 0x9999;
                }
            }    
        }
    }

    ptr = 0;
    int color = 0x3333;
    bool center = false;
    printString(setMap[0], 1*LETTER_HEIGHT, fb, color, center);
    printString(setMap[1], 3*LETTER_HEIGHT, fb, color, center);
    printString(setMap[2], 4*LETTER_HEIGHT, fb, color, center);
    
    redrawDisp(fb, base); //write data on display
}

/************************************************************
If theres a text diplayed on screen, this method can make
it flash red, this functionality is used when user selects
an option in menu

@stringOffset - particular 1D array in 2D array
@chunkOffset - row on display
@center - information about centering the text if theres an
odd number of symbols in it
*************************************************************/
void blink(int *stringOffset, int chunkOffset, unsigned short* fb, unsigned char* base, bool center)
{
    int color;
    for (int i = 0; i < 6; i++) //blink 3x
    { 
        if (i % 2 == 0)
        {
            color = 0x8800;
        } 
        else 
        {
            color = 0x3333;
        }
        printString(stringOffset, chunkOffset, fb, color, center);
        redrawDisp(fb, base); //write data on display
        usleep(65000);
    }
}

/************************************************************
This method converts a hexadecimal number into binary
represented as array

@number - the hex number
@line - the binary number
*************************************************************/
void convert(int number, int line[LETTER_WIDTH], int width, int height)
{
    int hex_lenght = LETTER_HEIGHT; 
    int a[hex_lenght], i, j;  

    //initialize arrays to 0 
    for(i = 0; i < width; i++)
    {
        a[i] = 0;
    }

    //convert to binary
    for(i = 0; i < height; i++)
    {    
        a[i] = number % 2; 
        number = number / 2;    
    }

    int leftSpacing = 2;

    //reverse order for representation
    i = hex_lenght - 1;
    for(j = leftSpacing; j < width; j++)
    {    
        line[j] = a[i];
        i--;
    }
}  
//---------------END_MENU_FUNC----------------