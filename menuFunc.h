/*******************************************************************
  File name:  menuFunc.h
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/

#ifndef __MENUFUNC_H__
#define __MENUFUNC_H__

#include <stdbool.h>

/**********************************************************
	This file holds major methods for text and map printing 
	@base = parlcd_mem_base adresses for display
	@fb = pixel values for display
***********************************************************/

/************************************************************
Utility methods for grid initialization 
*************************************************************/ 
void mapInit1(void);
void mapInit2(void);
void mapFree1(void);
void mapFree2(void);


/************************************************************
Uses information about lives from game.c
Returns pointer to array with encoded symbols 
*************************************************************/ 
int *sendHearts(void);
int *sendHeartsPlain(void);


/************************************************************
Calls function printString() based on @type, which is string
that says what to display.
*************************************************************/
void display(char *type, unsigned char* base, unsigned short* fb);


/************************************************************
Prints characters on display

@stringOffset - particular 1D array in 2D array
@chunkOffset - row on display
@center - information about centering the text if theres an
odd number of symbols in it
*************************************************************/
void printString(int *stringOffset, int chunkOffset, unsigned short* fb, int color, bool flag);


/************************************************************
Calls function blink() based on @type, which is string
that says what to display.

@option - particular 1D array in 2D array
*************************************************************/ 
void selection(char *type, int option, unsigned char* base, unsigned short* fb);


/************************************************************
Prints representation of maps

@mapNum - array of encoded map
*************************************************************/
void printMaps(int mapNum, unsigned short* fb, unsigned char* base);


/************************************************************
If theres a text diplayed on screen, this method can make
it flash red, this functionality is used when user selects
an option in menu

@stringOffset - particular 1D array in 2D array
@chunkOffset - row on display
@center - information about centering the text if theres an
odd number of symbols in it
*************************************************************/
void blink(int *stringOffset, int chunkOffset, unsigned short* fb, unsigned char* base, bool center);


/************************************************************
This method converts a hexadecimal number into binary
represented as array

@number - the hex number
@line - the binary number
*************************************************************/
void convert(int number, int line[10], int width, int height);

#endif