/*******************************************************************
  File name:  maps.c
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/

//Display 480x320

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "utils.h"
#include "grid.h"
#include "maps.h"
#include "gameAi.h"
#include "input.h"
#include "game.h"
#include "menuFunc.h"

#define DISP_XLEN 480
#define DISP_YLEN 320


void refreshPoints(unsigned short *fb, lineChunk* line, int i);

void genPointsInner(unsigned short *fb, lineChunk* line, int points,  int *counter);

void fillChunk(unsigned short *fb, int** idx , int chHeight, int chWidth);

void fillLine(unsigned short *fb, oneChunk* chunk, int chHeight,
			  int chWidth, int mapOf, int lineLast);
			  
void printMap(unsigned short *fb, lineChunk* map, locPlayer* player);

typedef struct
{
	int width;  //Number of cols of map rep
	int height; //Number of rows of map rep
	int* map;   //Selected map (1-3)
	int points; //Points on map
}localRep;

//Binary representation of maps (in Chunks) 1 = wall, 0 = free space, 2 = HP
int map1[] = {1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,
			  1,0,0,0,0,1,0,0,0,0,1,2,2,2,2,
			  1,0,1,1,0,0,0,1,1,0,1,1,1,1,1,
			  1,0,0,0,0,1,1,1,0,0,0,0,0,0,1,
			  1,1,1,0,1,1,0,0,0,1,1,0,1,0,1,
			  1,0,0,0,0,0,0,1,1,1,1,0,1,0,1,
			  1,0,1,1,1,1,0,0,0,0,0,0,1,1,1,
			  1,0,1,1,1,1,0,1,1,1,1,0,0,0,1,
			  1,0,0,0,0,0,0,0,0,0,0,0,1,0,1,
			  1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
			  
int map2[] = {1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,
			  1,0,0,1,0,0,0,0,1,0,1,2,2,2,2,
			  1,0,1,1,0,1,1,0,0,0,1,1,1,1,1,
			  1,0,0,0,0,0,1,0,1,0,0,0,0,1,1,
			  1,0,1,1,1,0,0,0,0,0,1,1,0,0,1,
			  1,0,1,0,1,0,1,1,1,0,1,1,1,0,1,
			  1,0,0,0,0,0,0,0,0,0,0,0,1,0,1,
			  1,0,1,1,1,0,1,1,0,0,1,0,0,0,1,
			  1,0,1,1,0,0,0,1,1,0,1,1,1,0,1,
			  1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,};
			
int map3[] = {1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,
			  1,0,0,0,0,0,0,0,0,0,1,2,2,2,2,
			  1,0,1,1,1,1,0,1,1,0,1,1,1,1,1,
			  1,0,1,1,1,1,0,1,1,0,1,1,1,0,1,
			  1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
			  1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
			  1,0,1,1,1,1,0,1,1,0,1,1,1,0,1,
			  1,0,1,1,1,1,0,1,1,0,1,1,1,0,1,
			  1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
			  1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,};
			  
localRep rep = {.width = 15, .height = 10, .map = map1};

/************************************************************
Function used to get local representation of maps

@mapNum = number of map representation
@ret    = pointer to map array
************************************************************/
int *getMaps(int mapNum){
    int *ret = 0;
    switch(mapNum){
        case 1:
            ret = map1;
            break;
        case 2:
            ret = map2;
            break;
        case 3:
            ret = map3;
            break;
        default:
            break;
    }
    return ret;
}

/************************************************************
Function used for printing one chunk line of map (row by row)

@fb       = array of RGB values of LCD display
@chunk    = array of chunks in current line, holding all disp. indexes
@chHeight = proportions of one chunk in y range
@chWidth  = proportions of one chunk in x range
@mapOf    = index of start of current printing line
@lineLast = index of start of previous line used for wall connection
************************************************************/
void fillLine(unsigned short *fb, oneChunk* chunk, int chHeight,
			  int chWidth, int mapOf, int lineLast)
{
	int offset = chunk[0].index[0][0];
	bool flag = false;
	if(offset == 0) //solve mem indexing for first line
	{
		lineLast = 0;
		flag = true;
	}
	int step = 0;
	int memOf = mapOf;
	int memLast = lineLast;
	for(int i = 0; i < chHeight -10; ++i)
	{
		mapOf = memOf;
		lineLast = memLast; //Reset of offsets for new line
		bool prev = false; //flag if previous wall was true
		for(int j = 0; j < DISP_XLEN/chWidth; ++j)
		{
			bool local = false;
			if((rep.map[lineLast] == 1 && rep.map[mapOf] == 1 && !flag) || i >= 10)
			{
				for (int k = 0; k < chWidth; ++k)
				{
					if((k >= 10 && k < chWidth - 10)&& rep.map[mapOf] == 1 && !prev)
					{
						//last chunk wasnt wall
						fb[offset+step] = 0x3303;
						local = true;
					}
					if(rep.map[mapOf] == 0)
					{
						fb[offset+step] = 0x0000;
						local = false;
					}
					if(rep.map[mapOf] == 1 && prev && (k < chWidth -10))
					{
						fb[offset+step] = 0x3303;
						local = true;
					}
					++step;
				}
			}
			else
			{
				step += chWidth;
			}
			prev = local;
			++mapOf;
			++lineLast;
		}
	}
}

/************************************************************
Function used to print single chunk (now used only for points)

@fb       = array of RGB values of LCD display
@idx      = 2D array holding all indexes of current chunk
@chHeight = proportions of one chunk in y range
@chWidth  = proportions of one chunk in x range
************************************************************/
void fillChunk(unsigned short *fb, int** idx , int chHeight, int chWidth)
{
	for(int i = 12; i < chHeight-12; ++i)
	{
		for (int j = 12; j < chWidth-12; ++j)
		{
			fb[idx[i][j]] = 0xe9e0;
		}
	}
}

/************************************************************
Function used to erase player or AI on map after move
Will not erase tile if on current position is someone else (colour based)
If after move is on tile still point (AI move), prints point again

@fb       = array of RGB values of LCD display
@idx      = 2D array holding all indexes of current chunk
@chHeight = proportions of one chunk in y range
@chWidth  = proportions of one chunk in x range
@player   = struct holding information about player or AI
************************************************************/
void erasePlayer(unsigned short *fb, int** idx , int chHeight, int chWidth, locPlayer* player)
{
	for(int i = 0; i < chHeight; ++i)
	{
		for (int j = 0; j < chWidth; ++j)
		{
			if(fb[idx[i][j]] == player->major || fb[idx[i][j]] == player->minor)
			{
				fb[idx[i][j]] = 0x0000;
			}
		}
	}
	int step = player->row*rep.width + player->col;
	if(rep.map[step] == 'p')
	{
		fillChunk(fb, idx , chHeight, chWidth);
	}
}

/************************************************************
Function used to print player or AI on map after move
If player (not AI) moves on tile with a point, point is collected

@fb       = array of RGB values of LCD display
@idx      = 2D array holding all indexes of current chunk
@chHeight = proportions of one chunk in y range
@chWidth  = proportions of one chunk in x range
@player   = struct holding information about player or AI
************************************************************/
void fillPlayer(unsigned short *fb, int** idx , int chHeight, int chWidth, locPlayer* player)
{
	for(int i = 0; i < chHeight; ++i)
	{
		for (int j = 0; j < chWidth; ++j)
		{
			if(i < j) //main diagonal
			{
				fb[idx[i][j]] = player->major;
			}
			else if(i > (chWidth -j)) //other diagonal
			{
				fb[idx[i][j]] = player->major;
			}
			else
			{
				fb[idx[i][j]] = player->minor;
			}
		}
	}
	int step = player->row*rep.width + player->col;
	if(player->major == 0x001e && rep.map[step] == 'p')
	{
		--rep.points;
		fprintf(stderr, "INFO: Points left on map: %d\n", rep.points);
		rep.map[step] = 0;
	}
}

/************************************************************
Function used to print map and everything needed when
game starts, or after player dies

@fb       = array of RGB values of LCD display
@map      = Initialized "Grid" struct - array, able to hold
            indexes of current "grid" line
@player   = struct holding information about player or AI
************************************************************/
void printMap(unsigned short *fb, lineChunk* map, locPlayer* player)
{
    int colour = 0x8800;
    bool center = false;
    
	for(int i = 0; i < map->yLen; ++i)
	{
		gridIndex(map,i);
		oneChunk *chunk = map->chunks;
		int mapOf = map->xLen * i;
		int lineLast = mapOf - map->xLen;
		fillLine(fb, chunk, map->chHeight, map->chWidth, mapOf, lineLast);
		if(i == 4)
		{
			int** idx = chunk[7].index;
			player->row = 4;
			player->col = 7;
			player->rowLast =4;
			player->colLast = 7;
			player->major = 0x001e;
			player->minor = 0x0360;
			fillPlayer(fb, idx , map->chHeight, map->chWidth, player);
		}
		
		if(sendLives() < 3)
		{
			refreshPoints(fb, map, i);
		}
	}
	printString(sendHearts(), 0, fb, colour, center);
}

/************************************************************
Function used to reprint uncollected points after player died

@fb   = array of RGB values of LCD display
@line = Initialized "Grid" struct - array, able to hold
            indexes of current "grid" line
@i    = current row on map
************************************************************/
void refreshPoints(unsigned short *fb, lineChunk* line, int i)
{
	int step = i*line->xLen;
	oneChunk *chunk = line->chunks;
	for(int j = 0; j < line->xLen; ++j)
	{
		if(rep.map[step++] == 'p')
		{
			int** idx = chunk[j].index;
			fillChunk(fb, idx , line->chHeight, line->chWidth);
		}
	}
}

/************************************************************
---------------------Set of MOVE functions-------------------
Used to check validity of move and its commitment

@fb       = array of RGB values of LCD display
@map      = Initialized "Grid" struct - array, able to hold
            indexes of current "grid" line
@player   = struct holding information about player or AI
************************************************************/
bool moveUp(lineChunk* map, unsigned short* fb, locPlayer* player)
{
	bool ret = false;
	int pos = (rep.width * player->row) + player->col;
	if(rep.map[pos-rep.width] == 1)
	{
		ret = true;
		return ret;	
	}
	player->rowLast = player->row;
	player->colLast = player->col;
	gridIndex(map,player->row);
	oneChunk *chunk = map->chunks;
	int** idx = chunk[player->col].index;
	erasePlayer(fb, idx , map->chHeight, map->chWidth, player);
	--player->row;
	gridIndex(map,player->row);
	chunk = map->chunks;
	idx = chunk[player->col].index;
	fillPlayer(fb, idx , map->chHeight, map->chWidth, player);
	return ret;
}

bool moveLeft(lineChunk* map, unsigned short* fb, locPlayer* player)
{
	bool ret = false;
	int pos = (rep.width * player->row) + player->col;
	if(rep.map[pos-1] == 1)
	{
		ret = true;
		return ret;	
	}
	player->rowLast = player->row;
	player->colLast = player->col;
	gridIndex(map,player->row);
	oneChunk *chunk = map->chunks;
	int** idx = chunk[player->col].index;
	erasePlayer(fb, idx , map->chHeight, map->chWidth, player);
	--player->col;
	idx = chunk[player->col].index;
	fillPlayer(fb, idx , map->chHeight, map->chWidth, player);
	return ret;
}

bool moveDown(lineChunk* map, unsigned short* fb, locPlayer* player)
{
	bool ret = false;
	int pos = (rep.width * player->row) + player->col;
	if(rep.map[pos+rep.width] == 1)
	{
		ret = true;
		return ret;	
	}
	player->rowLast = player->row;
	player->colLast = player->col;
	gridIndex(map,player->row);
	oneChunk *chunk = map->chunks;
	int** idx = chunk[player->col].index;
	erasePlayer(fb, idx , map->chHeight, map->chWidth, player);
	++player->row;
	gridIndex(map,player->row);
	chunk = map->chunks;
	idx = chunk[player->col].index;
	fillPlayer(fb, idx , map->chHeight, map->chWidth, player);
	return ret;
}

bool moveRight(lineChunk* map, unsigned short* fb, locPlayer* player)
{
	bool ret = false;
	int pos = (rep.width * player->row) + player->col;
	if(rep.map[pos+1] == 1)
	{
		ret = true;
		return ret;	
	}
	player->rowLast = player->row;
	player->colLast = player->col;
	gridIndex(map,player->row);
	oneChunk *chunk = map->chunks;
	int** idx = chunk[player->col].index;
	erasePlayer(fb, idx , map->chHeight, map->chWidth, player);
	++player->col;
	idx = chunk[player->col].index;
	fillPlayer(fb, idx , map->chHeight, map->chWidth, player);	
	return ret;
}
/************************************************************
------------------END of Set of MOVE functions---------------
************************************************************/

/************************************************************
Check presence of walls in surounding of player

@up, @last, @down, @left = values of directions
@map    = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
@player = struct holding information about player or AI
@ret    = Returns number of valid moves
************************************************************/
int checkValid(int* up, int* down, int* left, int* right, locPlayer* player, lineChunk* map)
{
	int valid = 4;
	int pos = (rep.width * player->row) + player->col;
	if(rep.map[pos-rep.width] == 1) //up
	{
		*up += -1;
		*down += -1;
		*left += -1;
		*right += -1;
		--valid;
	}
	if(rep.map[pos+rep.width] == 1)//down
	{
		*down += -2;
		*left += -1;
		*right += -1;
		--valid;
	}
	if(rep.map[pos-1] == 1) //left
	{
		*left += -3;
		*right += -1;
		--valid;
	}	
	if(rep.map[pos+1] == 1) //right
	{
		*right += -4;
		--valid;
	}
	return valid;
}

/************************************************************
Selects user chosen map in local representation

@line   = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
@map    = number of map that user selected
@player = struct holding information about player or AI
@ret    = Returns number of valid moves
************************************************************/
void initMap(unsigned short *fb, map_type map, lineChunk* line, locPlayer* player)
{
	switch(map)
	{
		case MAP_FIRST:
		{
			rep.map = map1;
			printMap(fb, line, player);
			break;
		}
		case MAP_SECOND:
		{
			rep.map = map2;
			printMap(fb, line, player);
			break;
		}
		case MAP_THIRD:
		{
			rep.map = map3;
			printMap(fb, line, player);
			break;
		}
	}
}
/************************************************************
Function used for generation and printing of points on map
Variable "safety" is used to limit infinite cycle (should not occur)

@fb     = array of RGB values of LCD display
@line   = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
@points = number of user chosed points
************************************************************/
void genPoints(unsigned short *fb, lineChunk* line, int points)
{
	int counter = 0;
	rep.points = 0;
	int safety = 0;
	while(rep.points < points && safety < 10)
	{
		genPointsInner(fb, line, points, &counter);
		++counter;
		++safety;
	}
	safety == 10 ? info("Safety net reached while generating poins!") : info("genPoints returned OK");
}

/************************************************************
Function used as inner generation of points
Points are not generated on starting positions and are generated
with offsets to generate points evenly

@fb     = array of RGB values of LCD display
@line   = Initialized "Grid" struct - array, able to hold
          indexes of current "grid" line
@counter= last state of offset
@points = number of user chosed points
************************************************************/
void genPointsInner(unsigned short *fb, lineChunk* line, int points,  int *counter)
{
	int pos = 0;
	for(int i = 0; i < rep.height; ++i)
	{
		for(int j = 0; j < rep.width ; ++j)
		{
			if(rep.points == points)
			{
				return;
			}
			if(!(i == 4 && j == 7) && !(i==1 && j == 1) && rep.map[pos] == 0)
			{
			 	*counter += 1;
				if(*counter % 5 == 0)
				{
					gridIndex(line,i);
					oneChunk *chunk = line->chunks;
					rep.map[pos] = 'p';
					rep.points += 1;
					int** idx = chunk[j].index;
					fillChunk(fb, idx , line->chHeight, line->chWidth);
				}
			}
			++pos;
		}
	}
}
/************************************************************
Function used to clean points from local map repr. after game end
************************************************************/
void cleanPoints(void)
{
	int step = 0;
	for(int i = 0; i < rep.width; ++i)
	{
		for(int j = 0; j < rep.height; ++j)
		{
			if(rep.map[step] == 'p')
			{
				rep.map[step] = 0;
			}
			++step;
		}
	}
}

/************************************************************
Function used to send points left on map to other files
************************************************************/
int sendPoints(void)
{
	return rep.points;
}

/************************************************************
Function used check proximity of 2 turns around player for
presence of AI
@player = struct holding information about player 
@player = struct holding information about AI
@ret    = true, if there is AI close to player, false otherwise
************************************************************/
bool checkSquare(locPlayer* player, locPlayer* ai)
{
	bool ret = false;
	int up = 0;
	int down = 1;
	int left = 2;
	int right = 3;
	checkValid(&up, &down, &left, &right, player, NULL);
	if(up >= 0)
	{
		if(player->row + 2 == ai->row && player->col == ai->col)
		{
			ret = true;
		}
	}
	if(down >=0)
	{
		if(player->row - 2 == ai->row && player->col == ai->col)
		{
			ret = true;
		}
	}
	if(left >= 0)
	{
		if(player->row  == ai->row && player->col -2 == ai->col)
		{
			ret = true;
		}
	}
	if(right >= 0)
	{
		if(player->row  == ai->row && player->col + 2 == ai->col)
		{
			ret = true;
		}
	}
	return ret;
}
//------------END_MAPS_C------------
