/*******************************************************************
  Project main function for MicroZed based MZ_APO board
  designed by Petr Porazil at PiKRON

  File name:  main.c
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/
#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "utils.h"
#include "grid.h"
#include "font_types.h"
#include "input.h"
#include "maps.h"
#include "ledLine.h"

unsigned short *fb;

#define DISP_XLEN 480
#define DISP_YLEN 320
#define ALLOCATION_ERROR 100
#define MAPPING_ERROR -1

int main(int argc, char *argv[]) {
	unsigned char *mem_base;
	unsigned char *parlcd_mem_base;
	unsigned short *fb  = (unsigned short *)malloc(DISP_YLEN * DISP_XLEN * sizeof(unsigned short));
	if(fb == NULL)
	{
	    exit(ALLOCATION_ERROR);
	}
  
	printf("Hello world\n");

  /*
   * Setup memory mapping which provides access to the peripheral
   * registers region of RGB LEDs, knobs and line of yellow LEDs.
   */
	mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

  /* If mapping fails exit with error code */
	if (mem_base == NULL)
	{
		exit(MAPPING_ERROR);
	}
  
	parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);

	if (parlcd_mem_base == NULL)
	{
	    exit(MAPPING_ERROR);
    }
    
	initLed(mem_base);
	turnOfRGB(); //Turn of RGB 
	parlcd_hx8357_init(parlcd_mem_base);
    eraseBuffer(fb); //set every member of fb to 0
    redrawDisp(fb, parlcd_mem_base); //set display black
	inputThr(parlcd_mem_base, fb); //"Program start"
	eraseBuffer(fb);
    redrawDisp(fb, parlcd_mem_base); //set display black
	printf("Goodbye world\n");
	turnOfRGB();
	turnOfLed();
	free(fb);
	return 0;
}