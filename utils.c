/*******************************************************************
  File name:  utils.c
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/
 
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <termios.h> 
#include <unistd.h>
#include "mzapo_parlcd.h"
#include "utils.h"

/************************************************************
Method for allocation with error handling
@size - amount of bytes that is beaing allocated
*************************************************************/
void* my_alloc(size_t size)
{
	void *ret = malloc(size);
	if(!ret)
	{
		fprintf(stderr, "ERROR: Cannot malloc\n");
		exit(101);
	}
	return ret;
}

/************************************************************
Method for switching to raw terminal
@reset - if 0 switch to raw, if 1 switch to normal
*************************************************************/
void call_termios(int reset)
{
	static struct termios tio, tioOld;
	tcgetattr(STDIN_FILENO, &tio);
	if (reset) {
	tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
	} else {
		tioOld = tio; //backup 
		cfmakeraw(&tio);
		tio.c_oflag |= OPOST;
		tcsetattr(STDIN_FILENO, TCSANOW, &tio);
   }
}

/************************************************************
Specialized printing methods
@str - string that is being printed
*************************************************************/
void info(const char* str)
{
	fprintf(stderr, "INFO: %s\n",str);
}
void debug(const char* str)
{
	fprintf(stderr, "DEBUG: %s\n",str);
}
void error(const char* str)
{
	fprintf(stderr, "ERROR: %s\n",str);
}
void warn(const char* str)
{
	fprintf(stderr, "WARN: %s\n",str);
}

/************************************************************
Methods for drawing on the board display
@parlcd_mem_base = adresses for display
@fb = pixel values for display
*************************************************************/
void redrawDisp(unsigned short *fb, unsigned char* parlcd_mem_base)
{
	parlcd_write_cmd(parlcd_mem_base, 0x2c);
	for (int ptr = 0; ptr < 480*320 ; ptr++) 
    {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
    }
}
void eraseBuffer(unsigned short *fb)
{
	for (int ptr = 0; ptr < 480*320 ; ptr++)
	{
		fb[ptr] = 0;

    }
}