/*******************************************************************
  File name:  utils.h
  Authors:	  Viktor Valik & Tomas Vayhel
  Date:		  20.5.2020
 *******************************************************************/
 
#include <unistd.h>

#ifndef __UTILS_H__
#define __UTILS_H__


/************************************************************
Method for allocation with error handling
@size - amount of bytes that is beaing allocated
*************************************************************/
void* my_alloc(size_t size);


/************************************************************
Method for switching to raw terminal
@reset - if 0 switch to raw, if 1 switch to normal
*************************************************************/
void call_termios(int reset);


/************************************************************
Specialized printing methods
@str - string that is being printed
*************************************************************/
void info(const char* str);

void debug(const char* str);

void error(const char* str);

void warn(const char* str);


/************************************************************
Methods for drawing on the board display
@parlcd_mem_base = adresses for display
@fb = pixel values for display
*************************************************************/
void eraseBuffer(unsigned short *fb);
void redrawDisp(unsigned short *fb, unsigned char* parlcd_mem_base);

#endif